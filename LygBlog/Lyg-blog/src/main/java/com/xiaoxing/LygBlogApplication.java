package com.xiaoxing;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@MapperScan("com.xiaoxing.mapper")
@EnableScheduling
@EnableSwagger2
public class LygBlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(LygBlogApplication.class, args);
        System.out.println("测试1");
    }

}
