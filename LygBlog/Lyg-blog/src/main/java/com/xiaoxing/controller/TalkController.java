package com.xiaoxing.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.Talk;
import com.xiaoxing.service.TalkService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @description:
 * @author: lyg
 * @create: 2024-03-06 14:16
 **/
@RestController
@RequestMapping("/talk")
@Api(value = "前台说说接口")
public class TalkController {
    @Autowired
    private TalkService talkService;

    @PostMapping("/add")
    public ResponseResult add(@RequestBody Talk talk){
        return ResponseResult.okResult(talkService.list());
    }
}
