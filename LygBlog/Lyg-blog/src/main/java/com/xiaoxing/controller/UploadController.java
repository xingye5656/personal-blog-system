package com.xiaoxing.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.User;
import com.xiaoxing.service.UploadService;
import com.xiaoxing.service.UserService;
import com.xiaoxing.utils.RedisCache;
import com.xiaoxing.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Api(tags = "上传图像")
public class UploadController {

    @Autowired
    private UploadService uploadService;

    @Autowired
    private UserService userService;



    @ApiOperation("oss上传")
    @PostMapping("/upload/{userId}")
    public ResponseResult uploadImg(@RequestParam("img") MultipartFile img, @PathVariable("userId") String userId){
        return uploadService.uploadImg(img,userId);
    }
}
