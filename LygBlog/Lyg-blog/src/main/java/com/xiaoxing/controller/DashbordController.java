package com.xiaoxing.controller;

import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.AdminView;
import com.xiaoxing.service.AdminViewService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: lyg
 * @create: 2024-02-29 16:48
 **/
@RestController
@RequestMapping("/dashbord")
@Api(value = "后台主页访问量接口")
public class DashbordController {
    @Autowired
    private AdminViewService adminViewService;

    //新增后台主页访问量
    @PostMapping
    public ResponseResult add(@RequestBody AdminView adminView){
        adminView.setAllView(0L);
        return ResponseResult.okResult(adminViewService.save(adminView));
    }

}
