package com.xiaoxing.controller;

import com.xiaoxing.constants.SystemConstants;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.dto.AddCommentDto;
import com.xiaoxing.domain.entity.Comment;
import com.xiaoxing.domain.entity.MessageCount;
import com.xiaoxing.service.CommentService;
import com.xiaoxing.service.MessageContService;
import com.xiaoxing.utils.BeanCopyUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/comment")
@Api(tags = "评论",description = "评论相关接口")//swagger2
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private MessageContService messageContService;

    @ApiOperation("评论分页")
    @GetMapping("/commentList")
    public ResponseResult commentList(Long articleId,Integer pageNum,Integer pageSize){
        return commentService.commentList(SystemConstants.ARTICLE_COMMENT,articleId,pageNum,pageSize);
    }

    @ApiOperation("添加评论")
    @PostMapping
    public ResponseResult addComment(@RequestBody AddCommentDto addCommentDto){
        Comment comment = BeanCopyUtils.copyBean(addCommentDto,Comment.class);
        return commentService.addComment(comment);
    }

    @GetMapping("/linkCommentList")
    @ApiOperation(value = "友链评论列表",notes = "获取一页友链评论")//接口描述配置
    @ApiImplicitParams({//接口参数描述
            @ApiImplicitParam(name = "pageNum",value = "页号"),
            @ApiImplicitParam(name = "pageSize",value = "每页大小")
    })
    public ResponseResult linkCommentList(Integer pageNum,Integer pageSize){
        return commentService.commentList(SystemConstants.LINK_COMMENT,null,pageNum,pageSize);
    }

}
