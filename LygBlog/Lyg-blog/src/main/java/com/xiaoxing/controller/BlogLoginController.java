package com.xiaoxing.controller;

import com.mysql.cj.util.StringUtils;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.User;
import com.xiaoxing.enums.AppHttpCodeEnum;
import com.xiaoxing.exception.SystemException;
import com.xiaoxing.service.BlogLoginService;
import com.xiaoxing.service.CodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "前台登录")
public class BlogLoginController {
    @Autowired
    private BlogLoginService blogLoginService;

    @Autowired
    private CodeService codeService;

    @ApiOperation("登录")
    @PostMapping("/login")
    public ResponseResult login(@RequestBody User user){
        if(StringUtils.isNullOrEmpty(user.getUserName())){
            //提示 必须传用户名
            throw new SystemException(AppHttpCodeEnum.REQUIRE_USERNAME);
        }
        return blogLoginService.login(user);
    }

    @ApiOperation("退出")
    @PostMapping("/logout")
    public ResponseResult logout(){
        return blogLoginService.logout();
    }

    /**
     * 给当前手机发送一条验证码
     * @param phonenumber 手机号
     * @return 发送结果
     * @throws Exception
     */
    @ApiOperation("给当前手机发送一条验证码")
    @GetMapping("/blog/sendcode/{phonenumber}")
    public ResponseResult sendCode(@PathVariable("phonenumber") String phonenumber) {
        return codeService.sendCode(phonenumber);
    }

    /**
     * 根据电话号码获取缓存中的验证码
     * @param phonenumber
     * @return
     */
    @ApiOperation("根据电话号码获取缓存中的验证码")
    @GetMapping("/blog/verifycode/{code}/{phonenumber}")
    public ResponseResult getCode(@PathVariable String code,@PathVariable String phonenumber) {
        return codeService.verifycode(code,phonenumber);
    }

}
