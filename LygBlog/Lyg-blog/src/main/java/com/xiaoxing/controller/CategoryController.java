package com.xiaoxing.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiaoxing.constants.SystemConstants;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.Category;
import com.xiaoxing.domain.vo.CategoryVo;
import com.xiaoxing.service.ArticleService;
import com.xiaoxing.service.CategoryService;
import com.xiaoxing.service.TagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/category")
@Api(tags = "目录")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TagService tagService;

    @Autowired
    private ArticleService articleService;

    @ApiOperation("目录查询")
    @GetMapping("/getCategoryList")
    public ResponseResult getCategoryList(){
        LambdaQueryWrapper<Category> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Category::getStatus, SystemConstants.NORMAL);
        List<Category> list1 = categoryService.list(wrapper);
        List<CategoryVo> list = new ArrayList<>();
        for (Category a: list1) {
            CategoryVo categoryVo = new CategoryVo();
            categoryVo.setId(a.getId());
            categoryVo.setName(a.getName());
            categoryVo.setDescription(a.getDescription());
            list.add(categoryVo);
        }
        return ResponseResult.okResult(list);
    }

    @ApiOperation("标签查询")
    @GetMapping("/getTagList")
    public ResponseResult getTagList(){
        return tagService.getTagList();
    }

    @ApiOperation("标签查询")
    @GetMapping("/getArticleListTag/{id}")
    public ResponseResult getArticleListTag(@PathVariable("id") Long id){
        return articleService.getArticleListTag(id);
    }


}
