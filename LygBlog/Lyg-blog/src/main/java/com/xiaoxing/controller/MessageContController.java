package com.xiaoxing.controller;

import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.MessageCount;
import com.xiaoxing.mapper.MessageCountMapper;
import com.xiaoxing.service.MessageContService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

/**
 * @description:
 * @author: lyg
 * @create: 2024-02-26 13:34
 **/
@RestController
@RequestMapping("/MessCont")
@Api(tags = "弹幕")
public class MessageContController {
    @Autowired
    private MessageContService messageContService;


    @ApiOperation("获得所有弹幕")
    @GetMapping("/getAllMess")
    public ResponseResult getMessCont(){
        List<MessageCount> list = messageContService.list();
        return ResponseResult.okResult(list);
    }

    @ApiOperation("添加弹幕")
    @PostMapping("/addMess")
    public ResponseResult addMess(@RequestBody MessageCount messageCount){
        UUID uuid = UUID.randomUUID();
        long mostSignificantBits = uuid.getMostSignificantBits();
        long leastSignificantBits = uuid.getLeastSignificantBits();
        long longUuid = mostSignificantBits ^ leastSignificantBits;
//        Boolean aBoolean = mapper.saveData(longUuid, messageCount.getMesAvatar(), messageCount.getMes(), messageCount.getMesTime(), messageCount.getMesStyle());
        return ResponseResult.okResult(messageContService.save(messageCount));
    }

}
