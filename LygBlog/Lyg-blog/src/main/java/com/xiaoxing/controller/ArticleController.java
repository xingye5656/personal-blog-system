package com.xiaoxing.controller;


import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/article")
@Api(tags = "文章接口")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

   /* @GetMapping("/list")
    public List<Article> test(){
        return articleService.list();
    }*/

    @ApiOperation("查询热门文章")
    @GetMapping("/hotArticleList")
    public ResponseResult hotArticleList() {

        // 封装成ResponseResult返回
        ResponseResult result = articleService.hotArticleList();
        return result;
    }


    @ApiOperation(value = "文章分页列表")
    @GetMapping("articleList")
    public ResponseResult articleList(Integer pageNum, Integer pageSize,Long categoryId) {

        return articleService.articleList(pageNum,pageSize,categoryId);
    }


    @ApiOperation(value = "文章详情")
    @GetMapping("/{id}")
    public ResponseResult getArticleDetail(@PathVariable("id") Long id){
        return articleService.getArticleDetail(id);
    }


    @ApiOperation(value = "更新文章浏览量")
    @PutMapping("/updateViewCount/{id}")
    public ResponseResult updateViewCount(@PathVariable("id") Long id){
        return articleService.updateViewCount(id);
    }


    @ApiOperation("得到文章的目录分组数据")
    @GetMapping("/getCategoryCount")
    public ResponseResult getCategoryCount(){
        return articleService.getCategoryCount();
    }
}
