package com.xiaoxing.controller;

import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.User;
import com.xiaoxing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/system/user")
public class SystemUserController {
    @Autowired
    private UserService userService;

    @GetMapping("list")
    public ResponseResult UserList(Integer pageNum, Integer pageSize, String userName, String status,String phonenumber){
        return userService.userList(pageNum,pageSize,userName,status,phonenumber);
    }

    @PostMapping
    public ResponseResult insertUser(@RequestBody User user){
        return userService.insertUser(user);
    }
    @DeleteMapping("{id}")
    private ResponseResult deleteUser(@PathVariable Long id){
        return userService.deleteUser(id);
    }

    @GetMapping("{id}")
    private ResponseResult userDetail(@PathVariable Long id){
        return userService.userDetail(id);
    }

    @PutMapping
    private ResponseResult updateUser(@RequestBody User user){
        return userService.updateUserInfo(user);
    }

}
