package com.xiaoxing.controller;

import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.dto.AddArticleDto;
import com.xiaoxing.domain.entity.Article;
import com.xiaoxing.service.ArticleService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/content/article")
@Api(tags = "后台文章接口")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @PostMapping
    public ResponseResult add(@RequestBody AddArticleDto article){
        return articleService.add(article);
    }

    @GetMapping("list")
    public ResponseResult selectArticle(Integer pageNum, Integer pageSize,String title,String summary){
        return articleService.selectArticle(pageNum,pageSize,title,summary);
    }

    @GetMapping("{id}")
    public ResponseResult selectArticleById(@PathVariable Long id){
        return articleService.selectArticleById(id);
    }

    @PutMapping
    public ResponseResult updateArticle(@RequestBody Article article){
        return articleService.updateArticle(article);
    }

    @DeleteMapping("{id}")
    public ResponseResult deleteArticle(@PathVariable Long id){
        return articleService.deleteArticle(id);
    }
}
