package com.xiaoxing.controller;

import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.dto.TagListDto;
import com.xiaoxing.domain.entity.LoginUser;
import com.xiaoxing.domain.entity.Tag;
import com.xiaoxing.domain.vo.PageVo;
import com.xiaoxing.domain.vo.TagVo;
import com.xiaoxing.service.TagService;
import com.xiaoxing.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/content/tag")
@Api(value = "后台标签接口")
public class TagController {
    @Autowired
    private TagService tagService;

    @ApiOperation(value = "查询标签列表")
    @GetMapping("/list")
    public ResponseResult<PageVo> list(Integer pageNum, Integer pageSize, TagListDto tagListDto){
        return tagService.pageTagList(pageNum,pageSize,tagListDto);
    }

    @ApiOperation(value = "插入标签列表")
    @PostMapping
    public ResponseResult insertTag(@RequestBody Tag tag){
        //通过token找到创建人
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long id = loginUser.getUser().getId();
        tag.setCreateBy(id);
        tag.setUpdateBy(id);
        //获取当前时间
        Date date = new Date();
        tag.setCreateTime(date);
        tag.setUpdateTime(date);
        //存到数据库中并返回
        return tagService.insertTag(tag);
    }

    @ApiOperation(value = "删除标签列表")
    @DeleteMapping("{id}")
    public ResponseResult deleteTag(@PathVariable("id") Long id){
        return tagService.deleteTag(id);
    }


    @GetMapping("{id}")
    public ResponseResult getTag(@PathVariable("id") Long id){
        return tagService.selectById(id);
    }

    @ApiOperation(value = "修改标签列表")
    @PutMapping
    public ResponseResult updateTag(@RequestBody TagVo tagVo){
        return tagService.updateTag(tagVo);
    }


    @GetMapping("/listAllTag")
    public ResponseResult listAllTag(){
        List<TagVo> list = tagService.listAllTag();
        return ResponseResult.okResult(list);
    }
}
