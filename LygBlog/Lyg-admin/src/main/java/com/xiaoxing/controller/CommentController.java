package com.xiaoxing.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xiaoxing.constants.SystemConstants;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.dto.CommentDto;
import com.xiaoxing.domain.vo.CommentPageVo;
import com.xiaoxing.service.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/comment")
@Api(tags = "评论",description = "评论相关接口")//swagger2
public class CommentController {

    @Autowired
    private CommentService commentService;

    @ApiOperation("评论分页")
    @PostMapping ("/commentList")
    public ResponseResult getCommentListPage(@RequestBody CommentDto dto){
        return ResponseResult.okResult(commentService.getCommentListPage(dto));
    }

    @DeleteMapping("/deleteValues")
    public ResponseResult deleteValues(@RequestBody List<Long> ids){
        List<Long>id=ids;
        return ResponseResult.okResult(commentService.removeByIds(id));
    }


}
