package com.xiaoxing.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiaoxing.constants.SystemConstants;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.*;
import com.xiaoxing.domain.vo.CategoryVo;
import com.xiaoxing.domain.vo.TagVo;
import com.xiaoxing.enums.AppHttpCodeEnum;
import com.xiaoxing.service.*;
import com.xiaoxing.service.impl.ArticleServiceImpl;
import com.xiaoxing.service.impl.CategoryServiceImpl;
import com.xiaoxing.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: lyg
 * @create: 2024-02-29 16:48
 **/
@RestController
@RequestMapping("/dashbord")
@Api(value = "后台主页接口")
public class DashbordController {
    @Autowired
    private AdminViewService adminViewService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageContService messageContService;

    @Autowired
    private ArticleServiceImpl articleImpl;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TagService tagService;

    //更新访问量
    @GetMapping("{userId}")
    public ResponseResult updateArticle(@PathVariable Long userId){
        LambdaQueryWrapper<AdminView>lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(AdminView::getUserId,userId);
        AdminView one = adminViewService.getOne(lambdaQueryWrapper);
        one.setAllView(one.getAllView()+1);
        adminViewService.updateById(one);
        return ResponseResult.okResult();
    }

    //得到访问量
    @GetMapping("/getAllView")
    public ResponseResult getAllView(){
        LoginUser loginUser = SecurityUtils.getLoginUser();
        LambdaQueryWrapper<AdminView>lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(AdminView::getUserId,loginUser.getUser().getId());
        List<AdminView> list = adminViewService.list(lambdaQueryWrapper);
        if (list.size()==0){
            return ResponseResult.okResult(0L);
        }else{
            return ResponseResult.okResult(list.get(0).getAllView());
        }
    }

    //得到文章总数
    @GetMapping("/getAllArticle")
    public ResponseResult getAllArticle(){
        int size = articleService.list().size();
        return ResponseResult.okResult(size);
    }

    //得到用户总数
    @GetMapping("/getAllUser")
    public ResponseResult getAllUser(){
        int size = userService.list().size();
        return ResponseResult.okResult(size);
    }

    //得到留言总数
    @GetMapping("/getAllMessageCount")
    public ResponseResult getAllMessageCount(){
        int size = messageContService.list().size();
        return ResponseResult.okResult(size);
    }

    @GetMapping("/getCategoryCount")
    public ResponseResult getCategoryCount(){
        return articleImpl.getCategoryCount();
    }


    @GetMapping("/getCategoryList")
    public ResponseResult getCategoryList(){
        LambdaQueryWrapper<Category>wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Category::getStatus, SystemConstants.NORMAL);
        List<Category> list1 = categoryService.list(wrapper);
        List<CategoryVo> list = new ArrayList<>();
        for (Category a: list1) {
            CategoryVo categoryVo = new CategoryVo();
            categoryVo.setId(a.getId());
            categoryVo.setName(a.getName());
            categoryVo.setDescription(a.getDescription());
            list.add(categoryVo);
        }
        return ResponseResult.okResult(list);
    }

    @GetMapping("/getTag")
    public ResponseResult getTag(){
        LambdaQueryWrapper<Tag>lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Tag::getDelFlag,"0");
        List<Tag> list = tagService.list(lambdaQueryWrapper);
        if (list.size()==0){
            return ResponseResult.okResult();
        }else {
            List<TagVo> tagVos=new ArrayList<>();
            for (Tag a:list) {
                TagVo tagVo=new TagVo();
                tagVo.setName(a.getName());
                tagVo.setColor("");
                tagVos.add(tagVo);
            }
            return ResponseResult.okResult(tagVos);
        }
    }

}
