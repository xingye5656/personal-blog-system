package com.xiaoxing.controller;

import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.Link;
import com.xiaoxing.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/content/link")
public class LinkController {
    @Autowired
    private LinkService linkService;

    @GetMapping("list")
    public ResponseResult linkList(Long pageNum,Long pageSize,String name,String status){
        return linkService.linkList(pageNum,pageSize,name,status);
    }

    @PostMapping
    public ResponseResult insertLink(@RequestBody Link link){
        return linkService.insertLink(link);
    }

    @PutMapping
    public ResponseResult updateLink(@RequestBody Link link){
        return linkService.updateLink(link);
    }

    @GetMapping("{id}")
    public ResponseResult linkById(@PathVariable Long id){
        return linkService.linkById(id);
    }

    @DeleteMapping("{id}")
    public ResponseResult deleteLink(@PathVariable Long id){
        return linkService.deleteById(id);
    }

}
