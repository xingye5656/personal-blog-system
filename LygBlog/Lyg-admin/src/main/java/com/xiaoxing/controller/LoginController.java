package com.xiaoxing.controller;

import com.mysql.cj.util.StringUtils;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.LoginUser;
import com.xiaoxing.domain.entity.User;
import com.xiaoxing.domain.vo.AdminUserInfoVo;
import com.xiaoxing.domain.vo.MenuVo;
import com.xiaoxing.domain.vo.RoutersVo;
import com.xiaoxing.domain.vo.UserInfoVo;
import com.xiaoxing.enums.AppHttpCodeEnum;
import com.xiaoxing.exception.SystemException;
import com.xiaoxing.service.CodeService;
import com.xiaoxing.service.LoginService;
import com.xiaoxing.service.MenuService;
import com.xiaoxing.service.RoleService;
import com.xiaoxing.utils.BeanCopyUtils;
import com.xiaoxing.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "后台登录接口")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private CodeService codeService;

    @PostMapping("/user/login")
    public ResponseResult login(@RequestBody User user){
        if(StringUtils.isNullOrEmpty(user.getUserName())){
            //提示 必须传用户名
            throw new SystemException(AppHttpCodeEnum.REQUIRE_USERNAME);
        }
        return loginService.login(user);
    }

    @GetMapping("getInfo")
    public ResponseResult<AdminUserInfoVo> getInfo(){
        //获取当前登录的用户
        LoginUser loginUser = SecurityUtils.getLoginUser();

        //根据用户id查询权限信息
        List<String> perms = menuService.selectPermsByUserId(loginUser.getUser().getId());
        //根据用户id查询角色信息
        List<String> roleKeyList = roleService.selectRoleKeyByUserId(loginUser.getUser().getId());

        //获取用户信息
        User user = loginUser.getUser();
        UserInfoVo userInfoVo = BeanCopyUtils.copyBean(user, UserInfoVo.class);

        //封装数据返回
        AdminUserInfoVo adminUserInfoVo = new AdminUserInfoVo(perms,roleKeyList,userInfoVo);
        return ResponseResult.okResult(adminUserInfoVo);
    }

    @GetMapping("getRouters")
    public ResponseResult<RoutersVo> getRouters(){
        Long userId = SecurityUtils.getUserId();
        //查询menu 结果是tree的形式
        List<MenuVo> menus = menuService.selectRouterMenuTreeByUserId(userId);
        //封装数据返回
        return ResponseResult.okResult(new RoutersVo(menus));
    }

    @PostMapping("/user/logout")
    public ResponseResult logout(){
        return loginService.logout();
    }

    /**
     * 给当前手机发送一条验证码
     * @param phone 手机号
     * @return 发送结果
     * @throws Exception
     */
    @PostMapping("/sendcode")
    public ResponseResult sendCode(@RequestParam String phone) {
        return codeService.sendCode(phone);
    }

    /**
     * 根据电话号码获取缓存中的验证码
     * @param phonenumber
     * @return
     */
    @ApiOperation("根据电话号码获取缓存中的验证码")
    @GetMapping("/blog/verifycode/{code}/{phonenumber}")
    public ResponseResult getCode(@PathVariable String code,@PathVariable String phonenumber) {
        return codeService.verifycode(code,phonenumber);
    }
}
