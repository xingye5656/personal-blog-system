package com.xiaoxing.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.dto.MessDto;
import com.xiaoxing.domain.dto.TalkDto;
import com.xiaoxing.domain.entity.MessageCount;
import com.xiaoxing.domain.entity.Talk;
import com.xiaoxing.service.MessageContService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description:
 * @author: lyg
 * @create: 2024-04-02 09:53
 **/

@RestController
@RequestMapping("/leaveWord")
@Api(tags = "留言",description = "留言相关接口")//swagger2
public class LeaveWordController {

    @Autowired
    private MessageContService messageContService;

    @ApiOperation("留言分页")
    @PostMapping("/list")
    public ResponseResult getTalkList(@RequestBody MessDto messDto){
        LambdaQueryWrapper<MessageCount> lambdaQueryWrapper=new LambdaQueryWrapper<>();
        if (messDto.getMes() != null && !messDto.getMes().isEmpty()) {
            lambdaQueryWrapper.like(MessageCount::getMes, messDto.getMes());
        }
        if (messDto.getMesTime() != null && !messDto.getMesTime().isEmpty()) {
            lambdaQueryWrapper.like(MessageCount::getMesTime, messDto.getMesTime());
        }
        if (messDto.getMesStyle() != null && !messDto.getMesStyle().isEmpty()) {
            lambdaQueryWrapper.like(MessageCount::getMesStyle, messDto.getMesStyle());
        }
        List<MessageCount> list = messageContService.list(lambdaQueryWrapper);
        int total = list.size(); // 获取总记录数
        // 计算分页起始索引和结束索引
        int startIndex = (messDto.getPageNum() - 1) * messDto.getPageSize();
        int endIndex = Math.min(startIndex + messDto.getPageSize(), total);

        // 截取分页数据
        List<MessageCount> pageData = list.subList(startIndex, endIndex);

        // 构建返回的分页对象
        Page<MessageCount> page = new Page<>();
        page.setCurrent(messDto.getPageNum());
        page.setSize(messDto.getPageSize());
        page.setRecords(pageData);
        page.setTotal(total);
        return ResponseResult.okResult(page);
    }

    @DeleteMapping("/deleteValues")
    public ResponseResult deleteValues(@RequestBody List<Long> ids){
        List<Long>id=ids;
        return ResponseResult.okResult(messageContService.removeByIds(id));
    }
}
