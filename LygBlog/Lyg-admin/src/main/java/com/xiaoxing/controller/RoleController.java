package com.xiaoxing.controller;

import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.Role;
import com.xiaoxing.domain.entity.RoleRequest;
import com.xiaoxing.service.RoleService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("system/role")
@Api(value = "角色接口")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("list")
    public ResponseResult AllRole(Integer pageNum,Integer pageSize,String RoleName,String status){
        return roleService.getAllRole(pageNum,pageSize,RoleName,status);
    }

    @PutMapping("changeStatus")
    public ResponseResult changeStatus(@RequestBody RoleRequest roleRequest){
        return roleService.changeStatus(roleRequest);
    }

    @PostMapping
    public ResponseResult insertRole(@RequestBody Role role){
        return roleService.insertRole(role);
    }

    @GetMapping("{id}")
    public ResponseResult getRole(@PathVariable Long id){
        return roleService.getRole(id);
    }

    @PutMapping
    public ResponseResult updateRole(@RequestBody Role role){
        return roleService.updateRole(role);
    }

    @DeleteMapping("{id}")
    public ResponseResult deleteRole(@PathVariable Long id){
        return roleService.deleteRole(id);
    }

    @GetMapping("listAllRole")
    public ResponseResult listAllRole(){
        return  roleService.listAllRole();
    }
}
