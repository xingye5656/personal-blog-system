package com.xiaoxing;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
@MapperScan("com.xiaoxing.mapper")

public class LygAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(LygAdminApplication.class, args);
        System.out.println("测试2");
    }

}
