package com.xiaoxing.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *control中注入service，service中注入mapper
 * 定义接口说明和参数说明

 * * 定义在类上： @Api
 *  *
 *  * 定义在方法上： @ApiOperation
 *  *
 *  * 定义在参数上： @ApiParam
 *  *
 *
 *
 * @author 李芋钢
 * @since
 */

//统一访问路径：http://localhost:7777/swagger-ui.html
//统一访问路径：http://localhost:8989/swagger-ui.html
@Configuration//配置类
@EnableSwagger2//swagger注解
public class SwaggerConfig {
    @Bean
    public Docket webApiConfig(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("webApi")
                .apiInfo(webApiInfo())
                .select()
                .paths(Predicates.not(PathSelectors.regex("/admin/.*")))
                .paths(Predicates.not(PathSelectors.regex("/error.*")))
                .build();
    }


    private ApiInfo webApiInfo(){
        return new ApiInfoBuilder()
                .title("个人博客中心API文档")
                .description("本文档描述了个人博客的定义")
                .version("1.0")
                .contact(new Contact("xiaoxing", "http://xiaoxing.com", "2780665379@qq.com"))
                .build();
    }
}
