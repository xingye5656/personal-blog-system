package com.xiaoxing.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxing.domain.entity.Menu;
import com.xiaoxing.domain.entity.MessageCount;

/**
 * @description:
 * @author: lyg
 * @create: 2024-02-26 13:36
 **/

public interface MessageCountMapper extends BaseMapper<MessageCount> {
    Boolean saveData(Long id,String mesAvatar,String mes,String mesTime,String mesStyle);
}
