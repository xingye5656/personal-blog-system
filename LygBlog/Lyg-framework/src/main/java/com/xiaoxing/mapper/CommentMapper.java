package com.xiaoxing.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxing.domain.entity.Comment;
import com.xiaoxing.domain.vo.CommentPageVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentMapper extends BaseMapper<Comment> {
    List<CommentPageVo> getAllListByDto(@Param("userName")String userName,@Param("articleName") String articleName,@Param("keyWord") String keyWord);
}
