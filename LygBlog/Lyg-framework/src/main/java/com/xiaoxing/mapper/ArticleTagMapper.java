package com.xiaoxing.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxing.domain.entity.ArticleTag;

public interface ArticleTagMapper extends BaseMapper<ArticleTag> {
}
