package com.xiaoxing.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxing.domain.entity.UserRole;

public interface UserRoleMapper extends BaseMapper<UserRole> {
}
