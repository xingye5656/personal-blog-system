package com.xiaoxing.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxing.domain.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
