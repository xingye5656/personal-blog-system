package com.xiaoxing.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxing.domain.entity.RoleMenu;

public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
}
