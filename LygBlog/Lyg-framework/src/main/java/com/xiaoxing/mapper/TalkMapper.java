package com.xiaoxing.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxing.domain.entity.Talk;

public interface TalkMapper extends BaseMapper<Talk> {
}
