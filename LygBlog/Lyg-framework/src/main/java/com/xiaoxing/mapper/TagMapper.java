package com.xiaoxing.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxing.domain.entity.Tag;
import com.xiaoxing.domain.vo.ArticleTagVo;
import com.xiaoxing.domain.vo.TagVo;

import java.util.List;

public interface TagMapper extends BaseMapper<Tag> {
    List<TagVo> getTageListNum();

}
