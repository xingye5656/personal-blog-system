package com.xiaoxing.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxing.domain.entity.AdminView;

public interface AdminViewMapper extends BaseMapper<AdminView> {
}
