package com.xiaoxing.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxing.domain.entity.Category;

public interface CategoryMapper extends BaseMapper<Category> {
}
