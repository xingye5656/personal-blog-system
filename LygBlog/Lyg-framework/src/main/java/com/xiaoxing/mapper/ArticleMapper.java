package com.xiaoxing.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoxing.domain.entity.Article;
import com.xiaoxing.domain.vo.ArticleTagVo;
import com.xiaoxing.domain.vo.CategoryCountVo;
import org.mapstruct.Mapper;

import java.util.List;


public interface ArticleMapper extends BaseMapper<Article> {
    List<CategoryCountVo> getCategoryCount();
    List<ArticleTagVo>getArticleTagVo(Long id);
    void updateViewCount(Long id,Long viewCount);
}
