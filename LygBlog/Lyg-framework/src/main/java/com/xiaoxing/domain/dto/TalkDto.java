package com.xiaoxing.domain.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lyg
 * @create: 2024-03-25 17:03
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "获得说说dto")//用于描述实体类
public class TalkDto {
    private Integer pageNum;
    private Integer pageSize;
    private String name;
    private String message;
}
