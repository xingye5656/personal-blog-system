package com.xiaoxing.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lyg
 * @create: 2024-02-26 13:31
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sg_message_count")
public class MessageCount {
    @TableId
    private Long id;

    //弹幕头像
    private String mesAvatar;

    //弹幕消息
    private String mes;

    //弹幕时间
    private String mesTime;

    //弹幕样式
    private String mesStyle;
}
