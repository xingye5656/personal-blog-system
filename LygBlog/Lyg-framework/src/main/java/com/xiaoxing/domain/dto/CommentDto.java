package com.xiaoxing.domain.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lyg
 * @create: 2024-03-06 15:56
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "获得评论dto")//用于描述实体类
public class CommentDto {
    private Integer pageNum;
    private Integer pageSize;
    private String keyWord;
    private String userName;
    private String articleName;
}
