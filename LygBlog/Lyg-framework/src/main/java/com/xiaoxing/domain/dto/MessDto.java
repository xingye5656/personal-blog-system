package com.xiaoxing.domain.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lyg
 * @create: 2024-04-02 10:06
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "获得留言dto")//用于描述实体类
public class MessDto {
    private Integer pageNum;
    private Integer pageSize;
    private String mes;
    private String mesTime;
    private String mesStyle;
}
