package com.xiaoxing.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @description:
 * @author: lyg
 * @create: 2024-02-24 22:09
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ArticleTagVo {
    //标题
    private String title;

    private Long id;
}
