package com.xiaoxing.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lyg
 * @create: 2024-02-24 13:44
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryCountVo {
    private Long categoryId;
    private int categoryCount;
}
