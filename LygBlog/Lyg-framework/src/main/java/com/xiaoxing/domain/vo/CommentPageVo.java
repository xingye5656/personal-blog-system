package com.xiaoxing.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lyg
 * @create: 2024-03-06 16:04
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentPageVo {

    private Long id;

    private String CommentUserName;

    private String comment;

    private String lastUserName;

    private String articleName;

    private String lastComment;

}
