package com.xiaoxing.domain.vo;

import com.xiaoxing.domain.entity.Role;
import com.xiaoxing.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailVo {
    private List<String> roleIds;
    private List<Role> roles;
    private User user;
}
