package com.xiaoxing.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @description:
 * @author: lyg
 * @create: 2024-02-29 17:00
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sg_admin_view")
@Accessors(chain = true) //支持map方法直接返回Article类型
public class AdminView {
    @TableId
    private Long id;

    private Long allView;

    private String userName;

    private Long userId;
}
