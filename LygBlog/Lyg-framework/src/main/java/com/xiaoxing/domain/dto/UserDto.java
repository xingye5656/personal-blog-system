package com.xiaoxing.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lyg
 * @create: 2024-03-25 14:43
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private String oldPassword;
    private String newPassword;
}
