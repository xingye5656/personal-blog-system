package com.xiaoxing.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoxing.constants.SystemConstants;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.Link;
import com.xiaoxing.domain.vo.LinkVo;
import com.xiaoxing.domain.vo.PageVo;
import com.xiaoxing.mapper.LinkMapper;
import com.xiaoxing.service.LinkService;
import com.xiaoxing.utils.BeanCopyUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("linkService")
public class LinkServiceImpl extends ServiceImpl<LinkMapper, Link> implements LinkService {
    /**
     *
     *   友联查询
     *
     **/
    @Override
    public ResponseResult getAllLink() {
        //查询所有审核通过的
        LambdaQueryWrapper<Link> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Link::getStatus, SystemConstants.LINK_STATUS_NORMAL);
        List<Link> links = list(queryWrapper);
        //转换成vo
        List<LinkVo> linkVos = BeanCopyUtils.copyBeanList(links, LinkVo.class);
        //封装返回
        return ResponseResult.okResult(linkVos);
    }

    /**
     *
     *   友联分页展示
     *
     **/
    @Override
    public ResponseResult linkList(Long pageNum, Long pageSize, String name, String status) {
        LambdaQueryWrapper<Link> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(!(name==""||name==null), Link::getName,name);
        wrapper.eq(!(status==""||status==null),Link::getStatus,status);
        //分页
        Page<Link> page = new Page<>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        page(page, wrapper);
        PageVo pageVo = new PageVo(page.getRecords(),page.getTotal());
        return ResponseResult.okResult(pageVo);
    }

    /**
     *
     *   添加友联
     *
     **/
    @Override
    public ResponseResult insertLink(Link link) {
        link.setDelFlag(0);
        save(link);
        return ResponseResult.okResult();
    }

    /**
     *
     *   跟新友联
     *
     **/
    @Override
    public ResponseResult updateLink(Link link) {
        updateById(link);
        return ResponseResult.okResult();
    }

    /**
     *
     *   根据id查询友联
     *
     **/
    @Override
    public ResponseResult linkById(Long id) {
        Link link = getById(id);
        return ResponseResult.okResult(link);
    }

    /**
     *
     *   删除友链
     *
     **/
    @Override
    public ResponseResult deleteById(Long id) {
        removeById(id);
        return ResponseResult.okResult();
    }
}
