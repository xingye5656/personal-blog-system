package com.xiaoxing.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.dto.CommentDto;
import com.xiaoxing.domain.entity.Comment;
import com.xiaoxing.domain.vo.CommentPageVo;

public interface CommentService extends IService<Comment> {
    ResponseResult commentList(String commentType, Long articleId, Integer pageNum, Integer pageSize);

    ResponseResult addComment(Comment comment);

    IPage<CommentPageVo> getCommentListPage(CommentDto dto);
}
