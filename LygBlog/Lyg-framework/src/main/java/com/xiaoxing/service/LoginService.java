package com.xiaoxing.service;

import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.User;

public interface LoginService {
    ResponseResult login(User user);
    ResponseResult logout();

}
