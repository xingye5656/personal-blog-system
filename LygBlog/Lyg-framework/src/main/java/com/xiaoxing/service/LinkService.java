package com.xiaoxing.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.Link;

public interface LinkService extends IService<Link> {
    ResponseResult getAllLink();

    ResponseResult linkList(Long pageNum, Long pageSize, String name, String status);

    ResponseResult insertLink(Link link);

    ResponseResult updateLink(Link link);

    ResponseResult linkById(Long id);

    ResponseResult deleteById(Long id);

}
