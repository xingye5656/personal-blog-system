package com.xiaoxing.service;

import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.User;

public interface BlogLoginService {
    ResponseResult login(User user);

    ResponseResult logout();

}
