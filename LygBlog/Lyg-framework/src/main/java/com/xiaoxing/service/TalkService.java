package com.xiaoxing.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxing.domain.entity.Talk;
import org.springframework.stereotype.Service;


public interface TalkService extends IService<Talk> {

}
