package com.xiaoxing.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxing.domain.entity.UserRole;

public interface UserRoleService extends IService<UserRole> {
}
