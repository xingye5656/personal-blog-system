package com.xiaoxing.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoxing.domain.entity.UserRole;
import com.xiaoxing.mapper.UserRoleMapper;
import com.xiaoxing.service.UserRoleService;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {
}
