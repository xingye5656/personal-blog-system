package com.xiaoxing.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxing.domain.entity.ArticleTag;

import java.util.List;

public interface ArticleTagService extends IService<ArticleTag> {

    List<ArticleTag> gettags(Long id);

}
