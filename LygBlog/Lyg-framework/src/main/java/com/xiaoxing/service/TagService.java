package com.xiaoxing.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.dto.TagListDto;
import com.xiaoxing.domain.entity.Tag;
import com.xiaoxing.domain.vo.PageVo;
import com.xiaoxing.domain.vo.TagVo;

import java.util.List;

public interface TagService extends IService<Tag> {
    ResponseResult<PageVo> pageTagList(Integer pageNum, Integer pageSize, TagListDto tagListDto);

    ResponseResult insertTag(Tag tag);

    ResponseResult deleteTag(Long id);

    ResponseResult selectById(Long id);

    ResponseResult updateTag(TagVo tagVo);

    List<TagVo> listAllTag();

    ResponseResult getTagList();
}
