package com.xiaoxing.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.dto.AddArticleDto;
import com.xiaoxing.domain.entity.Article;

public interface ArticleService extends IService<Article> {
    ResponseResult hotArticleList();

    ResponseResult articleList(Integer pageNum, Integer pageSize, Long categoryId);

    ResponseResult getArticleDetail(Long id);

    ResponseResult updateViewCount(Long id);

    ResponseResult add(AddArticleDto article);

    ResponseResult selectArticle(Integer pageNum, Integer pageSize, String title, String summary);

    ResponseResult selectArticleById(Long id);

    ResponseResult updateArticle(Article article);

    ResponseResult deleteArticle(Long id);

    ResponseResult getCategoryCount();

    ResponseResult getArticleListTag(Long id);
}
