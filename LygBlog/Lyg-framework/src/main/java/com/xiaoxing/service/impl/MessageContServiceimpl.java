package com.xiaoxing.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoxing.domain.entity.MessageCount;
import com.xiaoxing.mapper.MessageCountMapper;
import com.xiaoxing.service.MessageContService;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: lyg
 * @create: 2024-02-26 13:36
 **/
@Service("MessageContService")
public class MessageContServiceimpl extends ServiceImpl<MessageCountMapper, MessageCount> implements MessageContService {
}
