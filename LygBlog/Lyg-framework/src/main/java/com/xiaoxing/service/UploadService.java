package com.xiaoxing.service;

import com.xiaoxing.domain.ResponseResult;
import org.springframework.web.multipart.MultipartFile;

public interface UploadService {
    ResponseResult uploadImg(MultipartFile img,String userId);

}
