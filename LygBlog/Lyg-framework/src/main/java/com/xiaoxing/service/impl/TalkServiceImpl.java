package com.xiaoxing.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoxing.domain.entity.Talk;
import com.xiaoxing.mapper.TalkMapper;
import com.xiaoxing.service.TalkService;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: lyg
 * @create: 2024-03-06 14:24
 **/
@Service("TalkService")
public class TalkServiceImpl extends ServiceImpl<TalkMapper, Talk> implements TalkService {
}
