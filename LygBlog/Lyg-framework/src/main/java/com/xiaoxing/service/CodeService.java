package com.xiaoxing.service;

import com.xiaoxing.domain.ResponseResult;

/**
 * @Author: liyugang
 * Description: 发送短信业务接口
 * @Date: created on 2023/7/27
 */
public interface CodeService {
    ResponseResult sendCode(String phonenumber);
    ResponseResult verifycode(String code,String phonenumber);
}
