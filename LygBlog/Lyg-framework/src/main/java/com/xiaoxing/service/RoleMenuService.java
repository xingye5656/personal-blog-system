package com.xiaoxing.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxing.domain.entity.RoleMenu;

public interface RoleMenuService extends IService<RoleMenu> {
}
