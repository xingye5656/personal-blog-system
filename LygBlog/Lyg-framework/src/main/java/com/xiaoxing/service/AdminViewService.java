package com.xiaoxing.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxing.domain.entity.AdminView;

public interface AdminViewService extends IService<AdminView> {
}
