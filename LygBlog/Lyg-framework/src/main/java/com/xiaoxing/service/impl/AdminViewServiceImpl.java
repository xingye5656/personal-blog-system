package com.xiaoxing.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoxing.domain.entity.AdminView;
import com.xiaoxing.mapper.AdminViewMapper;
import com.xiaoxing.service.AdminViewService;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: lyg
 * @create: 2024-02-29 17:04
 **/
@Service
public class AdminViewServiceImpl extends ServiceImpl<AdminViewMapper, AdminView> implements AdminViewService {
}
