package com.xiaoxing.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxing.domain.entity.MessageCount;

public interface MessageContService extends IService<MessageCount> {
}
