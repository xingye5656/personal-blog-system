package com.xiaoxing.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaoxing.domain.entity.RoleMenu;
import com.xiaoxing.mapper.RoleMenuMapper;
import com.xiaoxing.service.RoleMenuService;
import org.springframework.stereotype.Service;

@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {
}
