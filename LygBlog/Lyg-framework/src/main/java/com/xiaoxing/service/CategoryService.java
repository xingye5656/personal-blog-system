package com.xiaoxing.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoxing.domain.ResponseResult;
import com.xiaoxing.domain.entity.Category;
import com.xiaoxing.domain.vo.CategoryVo;

import java.util.List;

public interface CategoryService extends IService<Category> {
    ResponseResult getCategoryList();

    List<CategoryVo> listAllCategory();


    ResponseResult pageList(Long pageNum, Long pageSize, String name, String status);

    ResponseResult insertCategory(Category category);

    ResponseResult selectCategory(Long id);

    ResponseResult updateCategory(Category category);

    ResponseResult deleteCategory(Long id);

}
