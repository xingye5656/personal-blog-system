package com.xiaoxing.utils;

import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 *  Bean的拷贝的工具类
 *
 * @author 李芋钢
 *
 */
public class BeanCopyUtils {

    private BeanCopyUtils() {
    }//私有的构造方法

    public static <V> V copyBean(Object source,Class<V> clazz) { //使用 泛型 的概念(即V代表某一不确定的类)
        //创建目标对象
        V result = null;
        try {
            result = clazz.newInstance();  //创建该类的对象
            //实现属性copy
            BeanUtils.copyProperties(source, result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //返回结果
        return result;
    }

    public static <O,V> List<V> copyBeanList(List<O> list, Class<V> clazz){ //双泛型使用
        //流转换方法
        return list.stream()
                .map(o -> copyBean(o, clazz))
                .collect(Collectors.toList());
    }

}
