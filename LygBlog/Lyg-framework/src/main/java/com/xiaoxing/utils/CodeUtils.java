package com.xiaoxing.utils;

import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;

/**
 * @Author: liyugang
 * Description: 验证码发送工具类
 * @Date: created on 2023/7/27
 */
public class CodeUtils {

    private static String accessKeyId="LTAI5tD6pJq3vat4jXVfTrzH"; // 阿里云accesskey
    private static String accessKeySecret="KaPYvlaOxxr8sU0G5x3HpSsBaIaAzR"; // 阿里云secret
    private static String templateCode="SMS_462300084"; // 阿里云短信模板id
    private static String signName="个人博客"; // 短信签名，【签名】

    /**
     * 使用AK&SK初始化账号Client
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new com.aliyun.dysmsapi20170525.Client(config);
    }

    /**
     * 发送验证码
     * @param phone 电话
     * @param code 验证码
     * @throws Exception 发送出错异常
     */
    public static void sendCode(String phone, String code) throws Exception {

        com.aliyun.dysmsapi20170525.Client client = CodeUtils.createClient( accessKeyId, accessKeySecret);
        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setPhoneNumbers(phone)
                .setSignName(signName)
                .setTemplateCode(templateCode)
                .setTemplateParam("{\"code\": \"" + code + "\"}");
        SendSmsResponse resp = client.sendSms(sendSmsRequest);
    }
}
