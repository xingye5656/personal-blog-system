package com.xiaoxing.utils;

import java.security.SecureRandom;
import java.util.Random;

/**
 * @Author: liyugang
 * Description: 生成6位验证码工具类
 * @Date: created on 2023/7/27
 */
public class RandomNumberUntils {
    private static final String SYMBOLS = "0123456789"; // 数字

    // 字符串
    // private static final String SYMBOLS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private static final Random RANDOM = new SecureRandom();

    public static String getCode() {

        // 如果需要4位，那 new char[4] 即可，其他位数同理可得
        char[] Code = new char[6];

        for (int index = 0; index < Code.length; ++index) {
            Code[index] = SYMBOLS.charAt(RANDOM.nextInt(SYMBOLS.length()));
        }

        return new String(Code);
    }

}
