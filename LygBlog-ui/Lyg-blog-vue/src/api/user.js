import request from '@/utils/request'

// 登录
export function userLogin(username,password) {
    return request({
        url: '/login',
        method: 'post',
        headers: {
            isToken: false
          },
        data: {'username':username,'password':password}
    })
}

export function userRegister(username,nickName,email,password) {
    return request({
        url: '/user/register',
        method: 'post',
        headers: {
            isToken :false
        },
        data: {"username":username,"nickName":nickName,"email":email,"password":password}
    })
}

//创建后台登录流量表
export function createAdminView(userInfo) {
  return request({
    url: '/dashbord',
    method: 'post',
    headers: {
      isToken :false
    },
    data: userInfo
  })
}


export function logout() {
    return request({
        url: '/logout',
        method: 'post'
    })
}

export function getUserInfo(userId) {
    return request ({
        url: '/user/userInfo',
        method: 'get',
        params: {"userId":userId}
    })
}

export function changePassword(UserDto) {
  return request({
    url: '/user/changeUserPassword',
    method: 'post',
    data: UserDto
  })
}


export function savaUserInfo(userinfo) {
    return request({
        url: '/user/userInfo',
        method: 'put',
        data: userinfo
    })
}

export function sendCodeToUser(phonenumber){
  return request({
    url: '/blog/sendcode/'+phonenumber,
    method: 'get',
  })
}

export function verifyCode(code,phonenumber){
  return request({
    url: '/blog/verifycode/'+code+'/'+phonenumber+'/',
    method: 'get',
  })
}
