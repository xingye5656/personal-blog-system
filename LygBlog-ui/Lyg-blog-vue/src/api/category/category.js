import request from '@/utils/request'

// 查询分类列表
export function getCategoryList() {
    return request({
        url: '/category/getCategoryList',
        headers: {
          isToken: false
        },
        method: 'get'
    })
}
// 查询文章分类个数
export function getCategoryCount() {
  return request({
    url: '/article/getCategoryCount',
    headers: {
      isToken: false
    },
    method: 'get'
  })
}

export function getTagListNum() {
  return request({
    url: '/category/getTagList',
    headers: {
      isToken: false
    },
    method: 'get'
  })
}

export function getArticleListTag(id) {
  return request({
    url: '/category/getArticleListTag/' + id,
    headers: {
      isToken: false
    },
    method: 'get'
  })
}


