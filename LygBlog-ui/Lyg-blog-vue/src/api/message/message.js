import request from '@/utils/request'

//获取所有弹幕
export function getMessageCont() {
  return request({
    url: '/MessCont/getAllMess',
    method: 'get'
  })
}


// 发送文章评论
export function sendMessageCont(querys) {
  return request({
    url: '/MessCont/addMess',
    method: 'post',
    data: querys
  })
}
