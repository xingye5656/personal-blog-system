// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import './assets/css/style.less'
import store from './store'
import MavonEditor from 'mavon-editor'
import { vueBaberrage } from 'vue-baberrage'
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(MavonEditor)
Vue.use(vueBaberrage)
import Waves from './components/Waves/index.vue'; // 引入你的组件



import '@fortawesome/fontawesome-free/css/all.css'; //在项目中引入 Font Awesome CSS 文件
// 定义全局组件
Vue.component('Waves', Waves);

import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  store
})
