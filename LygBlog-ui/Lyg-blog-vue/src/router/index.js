import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

export default new Router({
	scrollBehavior(to, from, savePosition) { // 在点击浏览器的“前进/后退”，或者切换导航的时候触发。
		if (savePosition) {
			return savePosition;
		} else {
            var top;
            if (window.innerWidth >= 700) {
                 top = 676
            } else {
                 top = 267
            }
			return {
				x: 0,
				y: top
			}
		}
	},
	routes: [{
			path: '/',
			component: resolve => require(['../views/home/Home.vue'], resolve),
			meta: {
				auth: true
			},
			name: 'Home'
		},
    //首页
		{
			path: '/Home',
			component: resolve => require(['../views/home/Home.vue'], resolve),
			meta: {
				auth: true
			},
			name: 'Home'
		},
    //分享详情
		{
			path: '/DetailArticle',
			component: resolve => require(['../views/DetailArticle.vue'], resolve),
			meta: {
				auth: true
			},
			name: 'DetailArticle'
		},
    //赞赏
		{
			path: '/Reward',
			component: resolve => require(['../views/Reward.vue'], resolve),
			meta: {
				auth: true
			},
			name: 'Reward'
		},
    //友链
		{
			path: '/FriendsLink',
			component: resolve => require(['../views/FriendsLink.vue'], resolve),
			meta: {
				auth: true
			},
			name: 'FriendsLink'
		},
    //注册登录
		{
			path: '/Login',
			component: resolve => require(['../views/login/Login.vue'], resolve),
			meta: {
				auth: false
			},
			name: 'Login'
		},
    //用户个人中心
		{
			path: '/UserInfo',
			component: resolve => require(['../views/UserInfo.vue'], resolve),
			meta: {
				auth: true
			},
			name: 'UserInfo'
		},
    //归档
    {
      path: '/archive',
      component: resolve => require(['../views/Archive/index.vue'], resolve),
      meta: {
        auth: true
      },
      name: '归档'
    },
    //标签
    {
      path: '/tag',
      component: resolve => require(['../views/Tag/index.vue'], resolve),
      meta: {
        auth: true
      },
      name: '标签'
    },
    //分类
    {
      path: '/Category',
      component: resolve => require(['../views/Category/index.vue'], resolve),
      meta: {
        auth: true
      },
      name: '分类'
    },
    //文章分类
    {
      path: '/Category/Share',
      component: resolve => require(['../views/Category/Share.vue'], resolve),
      meta: {
        auth: true
      },
      name: '分类文章跳转'
    },
    {
      path: '/ArticleTag',
      component: resolve => require(['../views/Tag/toArticle.vue'], resolve),
      meta: {
        auth: true
      },
      name: '标签文章跳转'
    },
    {
      path: '/Message',
      component: resolve => require(['../views/Message/index.vue'], resolve),
      meta: {
        auth: true
      },
      name: '标签文章跳转'
    },
    {
      path: '/talk',
      component: resolve => require(['../views/Talk/index.vue'], resolve),
      meta: {
        auth: true
      },
      name: '说说页面'
    },
	]
})
