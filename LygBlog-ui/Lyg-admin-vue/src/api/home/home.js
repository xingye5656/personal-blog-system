import request from '@/utils/request'
// 查询访问量
export function getAllView() {
  return request({
    url: '/dashbord/getAllView',
    method: 'get'
  })
}

// 查询文章总量
export function getAllArticle() {
  return request({
    url: '/dashbord/getAllArticle',
    method: 'get'
  })
}

// 查询用户个数
export function getAllUser() {
  return request({
    url: '/dashbord/getAllUser' ,
    method: 'get'
  })
}

//查询留言总量
export function getAllMessageCount() {
  return request({
    url: '/dashbord/getAllMessageCount' ,
    method: 'get'
  })
}

// 查询文章分类个数
export function getCategoryCount() {
  return request({
    url: '/dashbord/getCategoryCount',
    method: 'get'
  })
}

export function getCategoryList() {
  return request({
    url: '/dashbord/getCategoryList',
    method: 'get'
  })
}

export function getTag() {
  return request({
    url: '/dashbord/getTag',
    method: 'get'
  })
}
