import request from "@/utils/request";

export function leaveList(data) {
  return request({
    url: '/leaveWord/list',
    method: 'post',
    data: data
  })
}


export function delComment(ids) {
  return request({
    url: '/leaveWord/deleteValues',
    method: 'delete',
    data: ids
  })
}
