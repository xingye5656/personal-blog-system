import request from "@/utils/request";

export function listComment(data) {
  return request({
    url: '/comment/commentList',
    method: 'post',
    data: data
  })
}

export function delComment(ids) {
  return request({
    url: '/comment/deleteValues',
    method: 'delete',
    data: ids
  })
}
