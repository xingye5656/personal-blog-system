import request from "@/utils/request";

export function listTalk(data) {
  return request({
    url: '/talk/talkList',
    method: 'post',
    data: data
  })
}
